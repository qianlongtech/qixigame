/*globals $, alert, console, ActiveXObject, $$ */
var fieldCollection, field, posC, posR1, posR2, limit, startX, startY, varyX, varyY, startT, varyT;

function setArgs() {
    "use strict";
    fieldCollection = [
        [
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
            [0, 1, 1, 1, 0, 1, 1, 2, 1, 1, 0, 0, 0, 1, 0],
            [1, 1, 0, 1, 0, 1, 0, 2, 0, 1, 0, 1, 0, 1, 0],
            [0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 1, 0, 0, 0],
            [0, 1, 1, 1, 1, 1, 0, 2, 1, 1, 0, 1, 1, 1, 0],
            [0, 0, 0, 0, 0, 0, 0, 2, 0, 1, 0, 0, 0, 0, 0],
            [0, 1, 0, 1, 0, 1, 1, 2, 0, 1, 1, 1, 0, 1, 1],
            [0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 1, 0, 0, 0],
            [1, 1, 1, 1, 1, 1, 0, 2, 0, 1, 0, 1, 0, 1, 0],
            [0, 0, 0, 0, 0, 0, 0, 2, 0, 1, 0, 0, 0, 0, 0],
            [0, 1, 0, 1, 0, 1, 1, 2, 1, 1, 0, 1, 0, 1, 0],
            [0, 1, 0, 1, 0, 1, 0, 2, 0, 1, 1, 1, 1, 1, 0],
            [0, 0, 0, 1, 0, 0, 0, 2, 0, 0, 0, 1, 0, 0, 0],
            [0, 1, 1, 1, 0, 1, 1, 2, 1, 1, 0, 1, 0, 1, 1],
            [0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0],
            [1, 1, 0, 1, 1, 1, 0, 2, 0, 1, 1, 1, 1, 1, 0],
            [0, 0, 0, 1, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0],
            [0, 1, 0, 1, 0, 1, 0, 2, 1, 1, 0, 1, 0, 1, 1],
            [0, 0, 0, 0, 0, 1, 0, 2, 0, 0, 0, 0, 0, 0, 0]
        ],
        [
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0],
            [0, 0, 1, 0, 0, 1, 1, 2, 1, 1, 0, 0, 0, 1, 0],
            [1, 1, 1, 0, 1, 1, 0, 2, 0, 1, 0, 1, 0, 1, 0],
            [0, 0, 0, 0, 1, 0, 0, 2, 0, 0, 0, 1, 0, 0, 0],
            [0, 1, 1, 1, 1, 0, 1, 2, 0, 1, 0, 1, 1, 1, 0],
            [0, 0, 0, 0, 0, 0, 0, 2, 0, 1, 1, 1, 0, 0, 0],
            [0, 1, 1, 0, 1, 1, 0, 2, 0, 1, 0, 1, 0, 1, 1],
            [0, 0, 0, 0, 1, 0, 0, 2, 0, 0, 0, 0, 0, 1, 0],
            [1, 1, 0, 1, 1, 1, 0, 2, 1, 1, 0, 1, 1, 1, 0],
            [0, 1, 0, 0, 0, 0, 0, 2, 0, 1, 0, 1, 0, 0, 0],
            [0, 0, 0, 1, 0, 1, 1, 2, 0, 0, 0, 0, 0, 1, 0],
            [0, 1, 0, 1, 0, 1, 0, 2, 1, 1, 1, 0, 1, 1, 0],
            [0, 1, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0],
            [0, 1, 1, 0, 1, 1, 1, 2, 1, 1, 0, 1, 1, 1, 0],
            [0, 1, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 1, 0, 0],
            [0, 0, 0, 1, 1, 1, 0, 2, 0, 1, 1, 0, 1, 0, 1],
            [1, 1, 0, 0, 0, 1, 0, 2, 0, 0, 0, 0, 0, 0, 0],
            [0, 1, 1, 0, 1, 1, 1, 2, 0, 1, 0, 1, 0, 1, 1],
            [0, 0, 0, 0, 0, 0, 0, 2, 0, 1, 0, 0, 0, 0, 0]
        ],
        [
            [0, 1, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0],
            [0, 1, 0, 0, 0, 0, 1, 2, 1, 1, 0, 0, 0, 1, 0],
            [0, 1, 0, 1, 0, 1, 1, 2, 0, 1, 1, 1, 0, 1, 0],
            [0, 0, 0, 1, 0, 0, 0, 2, 0, 0, 0, 1, 0, 0, 0],
            [1, 1, 0, 0, 0, 1, 0, 2, 1, 1, 0, 1, 0, 1, 1],
            [0, 0, 0, 1, 0, 1, 0, 2, 0, 0, 0, 0, 0, 0, 0],
            [0, 1, 1, 1, 0, 1, 1, 2, 0, 1, 1, 0, 1, 1, 0],
            [0, 1, 0, 0, 0, 0, 0, 2, 0, 1, 0, 0, 0, 0, 0],
            [0, 1, 0, 1, 1, 1, 0, 2, 0, 0, 0, 1, 0, 1, 0],
            [0, 0, 0, 1, 0, 1, 0, 2, 1, 1, 0, 1, 1, 1, 1],
            [0, 1, 0, 0, 0, 1, 1, 2, 1, 0, 0, 0, 1, 0, 0],
            [0, 1, 1, 1, 0, 1, 0, 2, 1, 0, 1, 0, 1, 1, 0],
            [0, 0, 1, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0],
            [0, 1, 1, 0, 1, 0, 1, 2, 1, 1, 0, 1, 1, 0, 1],
            [1, 1, 0, 0, 1, 0, 0, 2, 0, 1, 0, 0, 0, 0, 0],
            [0, 1, 0, 1, 1, 1, 1, 2, 0, 1, 1, 1, 1, 1, 0],
            [0, 0, 0, 1, 0, 0, 0, 2, 0, 0, 0, 0, 0, 1, 0],
            [0, 1, 0, 0, 0, 1, 0, 2, 1, 1, 0, 1, 0, 1, 0],
            [0, 1, 0, 1, 0, 1, 0, 2, 0, 0, 0, 1, 0, 0, 0]
        ]
    ];
    field = fieldCollection[Math.floor(Math.random() * fieldCollection.length)];
    posC = {
        "r": 0,
        "c": 0
    };
    posR1 = {
        "r": 18,
        "c": 6
    };
    posR2 = {
        "r": 18,
        "c": 8
    };
}

function loadSource(showPage) {
    "use strict";
    var assets = {
        path: "img/",
        array: ["bggame.jpg",
                    "bg0.jpg",
                    "bg1.jpg",
                    "bg2.jpg",
                    "t0_1.png",
                    "t0_2.png",
                    "t0_3.png",
                    "t1_1.png",
                    "t1_2.png",
                    "t1_3.png",
                    "t2_1.png",
                    "share.png",
                    "cloud.png",
                    "control.png",
                    "ready.png",
                    "readygo.png",
                    "ready.gif",
                    "r1.gif",
                    "r2.gif",
                    "../mp3/bgm.mp3",
                    "../mp3/win.mp3",
                    "../mp3/fail.mp3"
                    ]
    };

    function loadImage(url, callback) {
        var o;
        if (window.XMLHttpRequest) {
            o = new XMLHttpRequest();
        } else if (window.ActiveXObject) {
            o = new ActiveXObject("Microsoft.XMLHTTP");
        }
        o.open("GET", url, true);
        o.send();
        o.onreadystatechange = function () {
            if (o.readyState === 4) {
                if (o.status === 200) {
                    callback("loaded");
                } else {
                    callback("error");
                }
            }
        };
    }

    function loadQue() {
        var n = assets.array.length;
        if (n === 0) {
            showPage(0);
            return false;
        }
        $.each(assets.array, function (index, item) {
            loadImage(assets.path + item, function (s) {
                if (s !== "error") {
                    n -= 1;
                } else {
                    alert("网速较慢，请刷新重试");
                }
                if (n === 0) {
                    showPage(0);
                }
            });
        });
    }
    loadQue();
}

function share() {
    "use strict";
    $("#share").toggle();
}

function showPage(x) {
    "use strict";
    var page = $("body .page:nth-child(" + (x + 1) + ")");
    page.siblings().hide();
    page.siblings().children().hide();
    page.show();
    page.children().each(function (index) {
        $(this).delay(300 * index).fadeIn();
    });
}

function draw(sth) {
    "use strict";
    var pos = [posC, posR1, posR2],
        cla = ["cloud", "r1", "r2"];
    $("#field").append('<div class="' + cla[sth] + '" style="top:' + (5.26 * pos[sth].r) + '%;left:' + (6.67 * pos[sth].c) + '%"></div>');
}

function drawField() {
    "use strict";
    $("#field").html("");
    $.each(field, function (nr, r) {
        $.each(r, function (nc, c) {
            posC = {
                "r": nr,
                "c": nc
            };
            if (c === 1) {
                draw(0);
            }
        });
    });
    draw(1);
    draw(2);
}

function changeField() {
    "use strict";

    function pos(value) {
        if (value) {
            value = 1;
            posC.r = 7;
            posC.c = 9;
            draw(0);
        } else {
            value = 0;
        }
    }
}

function main(direc) {
    "use strict";
    //c- r- c+ r+
    var pos = [posR1, posR2];

    function checkPos(r) {
        var standard;
        switch (direc) {
        case 0:
            if (pos[r].c === 0) {
                return false;
            }
            standard = field[pos[r].r][pos[r].c - 1];
            break;
        case 1:
            if (pos[r].r === 0) {
                return false;
            }
            standard = field[pos[r].r - 1][pos[r].c];
            break;
        case 2:
            if (pos[r].c === 14) {
                return false;
            }
            standard = field[pos[r].r][pos[r].c + 1];
            break;
        case 3:
            if (pos[r].r === 18) {
                return false;
            }
            standard = field[pos[r].r + 1][pos[r].c];
            break;
        }
        if (standard) {
            return false;
        } else {
            return true;
        }

    }

    function move(r) {
        switch (direc) {
        case 0:
            pos[r].c -= 1;
            break;
        case 1:
            pos[r].r -= 1;
            break;
        case 2:
            pos[r].c += 1;
            break;
        case 3:
            pos[r].r += 1;
            break;
        }
    }
    $("#field").find(".r1").remove();
    $("#field").find(".r2").remove();
    $.each(pos, function (r) {
        if (checkPos(r)) {
            move(r);
        }
        draw(r + 1);
        console.log(r + 1 + ":" + pos[r].r + " " + pos[r].c);
    });
    checkWin();
}

function onTouchStart(event) {
    "use strict";
    startX = event.targetTouches[0].clientX;
    startY = event.targetTouches[0].clientY;
}

function onTouchMove(event) {
    "use strict";
    varyX = event.targetTouches[0].clientX - startX;
    varyY = event.targetTouches[0].clientY - startY;
}

function onTouchEnd(event) {
    "use strict";
    var tan = Math.abs(varyY / varyX);
    if (varyX > 0 || varyY > 0) {
        if (varyX < 0 && tan < 1) {
            main(0);
        }
        if (varyY < 0 && tan > 1) {
            main(1);
        }
        if (varyX > 0 && tan < 1) {
            main(2);
        }
        if (varyY > 0 && tan > 1) {
            main(3);
        }
    }
}

function onTouch(on) {
    "use strict";
    if (on === 0) {
        startT = new Date().getTime();
        $("#control")[0].addEventListener("touchstart", onTouchStart);
        $("#control")[0].addEventListener("touchmove", onTouchMove);
        $("#control")[0].addEventListener("touchend", onTouchEnd);
        $("body").on("keydown", function (event) {
            main(event.which - 37);
        });
        $("body").on("touchmove", function () {
            return false;
        });
    } else {
        varyT = (new Date().getTime() - startT) / 1000;
        $("#time").html("只用了" + varyT + "s成功撮合一对儿！");
        $("#control")[0].removeEventListener("touchstart", onTouchStart);
        $("#control")[0].removeEventListener("touchmove", onTouchMove);
        $("#control")[0].removeEventListener("touchend", onTouchEnd);
        $("body").off("keydown");
        $("body").off("touchmove");
    }
}

function checkWin() {
    "use strict";
    if (posR1.r === 0 && posR1.c === 7 && posR2.r === 0 && posR2.c === 7) {
        $("audio").attr({
            "src": "mp3/win.mp3",
            "loop": false
        });
        showPage(1);
        onTouch(1);
        clearTimeout(limit);
    }
}

function checkFail() {
    "use strict";
    if (!(posR1.r === 0 && posR1.c === 7 && posR2.r === 0 && posR2.c === 7)) {
        $("audio").attr({
            "src": "mp3/fail.mp3",
            "loop": false
        });
        showPage(2);
        onTouch(1);
        clearTimeout(limit);
    }
}

function gameStart() {
    "use strict";
    $("audio").attr({
        "src": "mp3/bgm.mp3",
        "loop": true
    });
    limit = setTimeout(function () {
        checkFail();
    }, 124000);
}

function ready() {
    "use strict";
    $("#t3-1").show();
    $("#t3-2").css("background-image", "url(img/ready.gif)").show();
    setTimeout(function () {
        $("#t3-1").hide();
        $("#t3-2").hide();
        $("#t3-3").show();
    }, 3000);
    setTimeout(function () {
        $("#t3-3").hide();
        onTouch(0);
    }, 4000);
    gameStart();
}

function guide() {
    "use strict";
    if ($("#guide").hasClass("first")) {
        $("#guide").show();
        $("#guide").click(function () {
            $("#guide").hide();
            ready();
        });
        $("#guide").removeClass("first");
    } else {
        ready();
    }
}

function gameEnter() {
    "use strict";
    showPage(3);
    setArgs();
    drawField();
    guide();
}

$(function () {
    "use strict";
    loadSource(showPage);
    $("#t0-3").click(gameEnter);
    $("#t1-3").click(gameEnter);
    $("#t2-3").click(gameEnter);
    $("#t1-2").click(share);
    $("#t2-2").click(share);
    $("#share").click(share);
});